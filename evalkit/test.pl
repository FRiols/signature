#!/usr/bin/perl
use strict;

open(FIN,"$ARGV[0]");
open(FOUT,">$ARGV[1]");

while(<FIN>)
  {
    my $s = rand(10) - 5;
    my $t ="f";
    if ($s >= 0)
      {
	$t = "t";
      }
    chomp;
    print FOUT "$_ ${s} ${t}\n";
  }
close(FIN);
close(FOUT);
