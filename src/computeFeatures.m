function FEATURES = computeFeatures(data)
%% Compute features of a signature

FEATURES.speed = computeSpeed(data);
FEATURES.meanSpeed = mean(FEATURES.speed);
FEATURES.acc = computeAcceleration(FEATURES.speed, data.time);
FEATURES.meanAcceleration = mean(FEATURES.acc);
FEATURES.angles = computeAngles(data.x, data.y);
FEATURES.sumAngles = sum(FEATURES.angles);
FEATURES.dpd = computeDistanceFirstLastPoint(data.x, data.y);
FEATURES.dm = computeMassDimension(data.x, data.y);
FEATURES.sp = computeSlowPoints(data.x, data.y, data.time);
FEATURES.lengthSign = computeLengthSignature(data.x, data.y);
FEATURES.rgd = computeLeftRightRatio(data.x);
FEATURES.rhb = computeTopBottomRatio(data.y);
FEATURES.rxy = computeXYRatio(data.x, data.y);
FEATURES.apd = computeFirstLastAngles(data.x, data.y);
FEATURES.horzMouv = computeHorizontalMouv(data.x);
FEATURES.vertMouv = computeVerticalMouv(data.y);
FEATURES.totalTime = computeTotalTime(data.time);
FEATURES.highPressure = computeHighPressurePoints(data.x, data.y, data.pressure);

end