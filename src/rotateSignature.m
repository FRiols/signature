function rotateSignature(data)

varX = var(data.x)
varY = var(data.y)
covXY = cov(data.x, data.y)

D = sqrt((varX - varY)^2 + 4 * covXY)

a = sqrt(1/2 + (varX - varY) / (2 * D))
b = (covXY ./ abs(covXY)) * sqrt(1/2 - (varX - varY) / (2 * D))

end