function geneticAlgorithm(kept, generated, iterationsPerGen)

addpath('Features')
numFeatures = 4;

possibleTrainSize = [1 3 6];
weights = ones(kept + generated, numFeatures);   % Stores all weights
dataVector = cell(2);                            % Used as a temporary buffer for data

genNumber = 0;
while true
    genNumber = genNumber + 1;
    scoreThisGen = 0;
    
    % Choose a train size
    trainSize = possibleTrainSize(randi([1, 3]));
    
    % Build your new generation using crossover and mutation
    for newIndividual = kept + 1 : kept + generated
        for feature = 1 : numFeatures
            weights(newIndividual, feature) = weights(randi([1, kept]), feature) * (0.8 + (0.4).*rand()); % Crossover and mutation
        end
    end
    
    % Reset the scores
    scores = zeros(kept + generated, 1);       % Stores how well each individual performed
    
    % Calculate the score for each individual of the current iteration
    for iteration = 1 : iterationsPerGen
        userForThisIteration = randi([1, 5]);
        
        % Train the average features
        avgGoodMeanSpeed = 0;
        avgGoodMeanAcceleration = 0;
        avgGoodTotalTime = 0;
        avgGoodXYRatio = 0;
        avgBadMeanSpeed = 0;
        avgBadMeanAcceleration = 0;
        avgBadTotalTime = 0;
        avgBadXYRatio = 0;
        for train = 1 : trainSize
            % Good data (1 - 20)
            fileID = fopen(strcat('../sample/USER', int2str(userForThisIteration), '_', int2str(randi([1, 20])), '.txt'), 'r');
            data = loadfile(fileID);
            data = normalizeData(data);
            dataVector{1} = data;               % Store the coordinates (x, y), time, status, azimuth, altitude and pressure
            features = computeFeatures(data);   % Compute and store features
            
            avgGoodMeanSpeed = avgGoodMeanSpeed + features.meanSpeed;
            avgGoodMeanAcceleration = avgGoodMeanAcceleration + features.meanAcceleration;
            avgGoodTotalTime = avgGoodTotalTime + features.totalTime;
            avgGoodXYRatio = avgGoodXYRatio + features.rxy;
            
            % Bad data (21 - 40)
            fileID = fopen(strcat('../sample/USER', int2str(userForThisIteration), '_', int2str(randi([21, 40])), '.txt'), 'r');
            data = loadfile(fileID);
            data = normalizeData(data);
            dataVector{1} = data;               % Store the coordinates (x, y), time, status, azimuth, altitude and pressure
            features = computeFeatures(data);   % Compute and store features
            
            avgBadMeanSpeed = avgBadMeanSpeed + features.meanSpeed;
            avgBadMeanAcceleration = avgBadMeanAcceleration + features.meanAcceleration;
            avgBadTotalTime = avgBadTotalTime + features.totalTime;
            avgBadXYRatio = avgBadXYRatio + features.rxy;
        end
        avgGoodMeanSpeed = avgGoodMeanSpeed / trainSize;
        avgGoodMeanAcceleration = avgGoodMeanAcceleration / trainSize;
        avgGoodTotalTime = avgGoodTotalTime / trainSize;
        avgGoodXYRatio = avgGoodXYRatio / trainSize;
        avgBadMeanSpeed = avgBadMeanSpeed / trainSize;
        avgBadMeanAcceleration = avgBadMeanAcceleration / trainSize;
        avgBadTotalTime = avgBadTotalTime / trainSize;
        avgBadXYRatio = avgBadXYRatio / trainSize;
        
        % Run the blind tests and see how well they score
        for individual = 1 : kept + generated
            chosenImage = randi([1, 40]);
            fileID = fopen(strcat('../sample/USER', int2str(userForThisIteration), '_',  int2str(chosenImage), '.txt'), 'r');
            data = loadfile(fileID);
            data = normalizeData(data);
            dataVector{1} = data;               % Store the coordinates (x, y), time, status, azimuth, altitude and pressure
            features = computeFeatures(data);   % Compute and store features
            
            diffToGood = weights(individual,1) * (features.meanSpeed - avgGoodMeanSpeed)^2 + weights(individual,2) * (features.meanAcceleration - avgGoodMeanAcceleration)^2 + weights(individual,3) * (features.totalTime - avgGoodTotalTime)^2 + weights(individual,4) * (features.rxy - avgGoodXYRatio)^2;
            diffToBad = weights(individual,1) * (features.meanSpeed - avgBadMeanSpeed)^2 + weights(individual,2) * (features.meanAcceleration - avgBadMeanAcceleration)^2 + weights(individual,3) * (features.totalTime - avgBadTotalTime)^2 + weights(individual,4) * (features.rxy - avgBadXYRatio)^2;
            if diffToGood < diffToBad
                if chosenImage <= 20
                    scores(individual) = scores(individual) + 1;
                end
            else
                if chosenImage > 20
                    scores(individual) = scores(individual) + 1;
                end
            end
        end
    end
    
    % Keep only the best scores
    keptWeights = zeros(kept, numFeatures);
    backupScores = zeros(kept +  generated, 1);
    for keptIndividual = 1 : kept
        [maxVal, maxInd] = max(scores);
        scoreThisGen = scoreThisGen + maxVal;
        backupScores(keptIndividual) = scores(maxInd);
        scores(maxInd) = -1;
        for feature = 1 : numFeatures
            keptWeights(keptIndividual, feature) = weights(maxInd, feature);
        end
    end
    for keptIndividual = 1 : kept
        for feature = 1 : numFeatures
            weights(keptIndividual, feature) = keptWeights(keptIndividual, feature);
        end
    end
    
    % Display average score
    fprintf('\nSuccess rate at gen %d: %.2f%%\n', genNumber, scoreThisGen * 100 / (kept * iterationsPerGen));
    % Display best weight distribution
    fprintf('Weight distributions:\n');
    for i = 1 : kept + generated
        fprintf('%.2f \t%.2f \t%.2f \t%.2f \t\tscore: %.2f%%\n', weights(i, 1), weights(i, 2), weights(i, 3), weights(i, 4), backupScores(i) * 100 / iterationsPerGen);
    end
    
    % Back for another loop
end

end