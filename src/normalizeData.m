function data = normalizeData(data)
%% Normalize the data: rotation, homothetie and translation.

    %% Rotation
    p = polyfit(data.x, data.y, 1);
    
    minX = min(data.x);
    maxX = max(data.x);
    avgX = (minX + maxX) / 2;
    data.x = data.x - avgX;
    data.y = data.y - (p(2) + avgX * p(1));
    
    rotAngle = -atan2(p(1), 1);
    xRot     = data.x * cos(rotAngle) - data.y * sin(rotAngle);
    yRot     = data.x * sin(rotAngle) + data.y * cos(rotAngle);
    
    data.x = xRot;
    data.y = yRot;
    
    %% Translation
    minX = min(data.x);
    maxX = max(data.x);
    minY = min(data.y);
    maxY = max(data.y);
    
    ratio = max(maxX - minX, maxY - minY) / 2;

    data.x = (data.x - (minX + maxX) / 2) / ratio;
    data.y = (data.y - (minY + maxY) / 2) / ratio;
end