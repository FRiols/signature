function script(userNum, useProfiler)

addpath('Features')
close all;

% By default, test user 1 and don't use the profiler
if nargin < 2
    if nargin < 1
        userNum = 1;
    end
    useProfiler = false;
    disp('Usage: script([userNumber], [useProfiler]), default arguments are 1 and false');
end

if useProfiler
    profile on -timer 'real';
else
    tic;
end

% The images 1 2 3 4 are from the same man, The images 21 22 23 24 are
% impostors ! There is different signatures so we can display different
% kind of signature.
images = [1 2 3 4 21 22 23 24];

% Structures initialisation
dataVector = cell(8, 2);

speed = zeros(8, 1);
acc = zeros(8, 1);
angles = zeros(8, 1);
horzMouv = zeros(8, 1);
vertMouv = zeros(8, 1);
time = zeros(8, 1);

% Compute features for each signature
for n = 1:length(images)
    % Open the file
    fileID = fopen(strcat('../sample/USER', int2str(userNum), '_', int2str(images(n)), '.txt'), 'r');
    if fileID == -1
        disp(strcat('../sample/USER', int2str(userNum), '_', int2str(images(n)), '.txt could not be opened'));
        return;
    end

    % Load and normalize data
    data = loadfile(fileID);
    data = normalizeData(data);

    % Store the coordinates (x, y), time, status, azimuth, altitude and
    % pressure
    dataVector{n, 1} = data;

    % Compute and store features
    features = computeFeatures(data);
    dataVector{n, 2} = features;

    % Variables used to be print (debug)
    speed(n) = features.meanSpeed;
    acc(n) = features.meanAcceleration;
    angles(n) = features.sumAngles;
    t(n) = features.totalTime;

    %pause;
end


%plotSignature(data);
plotMultiple(dataVector, size(dataVector, 1));

if useProfiler
    profile viewer;
else
    toc;
end

end
