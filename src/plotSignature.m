function plotSignature(data)

figure;
plot(data.x, data.y, 'LineWidth', 5);

hold on;
plot(data.x, data.y, 'ro', 'LineWidth', 3);

end