function d = computeDTW(s, t)
    ind = isnan(s);
    s(ind) = 0;
    ind = isnan(t);
    t(ind) = 0;
    n = length(s);
    m = length(t);
    if n > 2 * m || m > 2 * n
        d = inf;
        return;
    end;
    w = max(idivide(min(n, m), int32(2)), abs(n - m));
    dtw = inf * ones(n + 1, m + 1);
    dtw(1, 1) = 0;
    costs = sqrt((repmat(s(:,1), 1, m) - repmat(t(:,1)', n, 1)) .^ 2 + ...
            (repmat(s(:,2), 1, m) - repmat(t(:,2)', n, 1)) .^ 2);
    for i = 1:n
        for j = max(1, i - w):min(m, i + w)
            dtw(i + 1,j + 1) = costs(i, j) + min([dtw(i,j + 1), dtw(i + 1, j), dtw(i, j)]);
        end
    end
    d = dtw(n + 1, m + 1);
end