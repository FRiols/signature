function plotMultiple(dataVector, num)

dataCount = size(dataVector);

figure('units', 'normalized', 'outerposition', [0 0 1 1]);

for i = 1 : dataCount
    data = dataVector{i, 1};
    %features = dataVector{i, 2};
    
    if num == 6
        subplot(2, 3, i);
    else
        subplot(2, 4, i);
    end

    plot(data.x, data.y, 'LineWidth', 5);
    hold on;
    plot(data.x, data.y, 'ro', 'LineWidth', 0.5);
    %hold on;
    %plot(data.x(features.sp), data.y(features.sp), 'gx', 'LineWidth', 4);
    %hold on;
    %plot(data.time, data.x, 'ro', 'LineWidth', 4);
    %hold on;
    %plot(data.time, data.y, 'bo', 'LineWidth', 4);
end

end