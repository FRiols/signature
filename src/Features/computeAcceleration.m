function acc = computeAcceleration(speed, time)

acc = zeros(length(speed), 1);

for i = 2:length(speed)
    acc(i) = (speed(i - 1) - speed(i))^2 + (speed(i - 1) - speed(i))^2;
    dt = time(i) - time(i - 1);
    if (dt == 0)
        dt = 10;
    end
    acc(i) = sqrt(acc(i)) / dt;
end

end