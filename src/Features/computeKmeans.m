
function [IDX, C] = computeKmeans(dataVector, nbclasse)
    data = [];
    for i = 1:length(dataVector)
        newd = [dataVector{i}.x, dataVector{i}.y];
        data = [data; newd];
    end
    nbc = 1;
    C = mean(data);

    for i = 1:log2(nbclasse)
        if i == 1
            C = split(C);
        else
            C = intelSplit(C, IDX, nbc);
        end
        nbc = nbc * 2;

        %disp(['KMEANS with ' num2str(nbc) ' classes ....................................................']);
        if (i ~= 8)
            % Select 10% of data
            %ind = rand(size(data, 1), 1) < .1;
            [IDX, C] = kmeans(data, nbc, 'Start', C, 'MaxIter', 500, ...
                'Options', statset('UseParallel', 1));
        else
            disp('Computing a new cluster with full data...........................................');
            [IDX, C] = kmeans(data, nbc, 'Start', C, 'MaxIter', 1000, ...
                'OnlinePhase', 'off', 'Options', statset('UseParallel', 1));
        end
    end
end

function script()
    % Load the Data (with discretization by K-means)

    %[CodeTrain, LabelTrain, Code] = loadTrainData();
    %[CodeTest, LabelTest] = loadTestData(Code);
    %save('kmdata.mat');

    % Train the HMM

    %load('kmdata.mat');
    %[TRANS, EMIS] = trainHMM(25, CodeTrain, LabelTrain);
    %save('res.mat');

    % Test the HMM

    %load('res.mat');
    %testHMM(LabelTest, CodeTest, TRANS, EMIS)

    % Load the raw data (2D points)

    %[TRAIN, LABEL_TRAIN] = loadUnipenData('pendigits-orig.tra');
    %[TEST, LABEL_TEST] = loadUnipenData('pendigits-orig.tes');
    %save('data.mat');

    % Test the DTW

    %load('data.mat');
    %testDTW(TRAIN, LABEL_TRAIN, TEST, LABEL_TEST)

end

function [CodeTrain, LabelTrain, Code] = loadTrainData() %#ok<*DEFNU>
    disp('Load Train Data ...')
    [TRAIN, LabelTrain] = loadUnipenData('pendigits-orig.tra');

    disp('Normalise Train data ...');
% Normalize Data and built KMEAN Training matrix
    kmeansTrain = [];
    for i = 1:length(TRAIN)
        idx = find(not(isnan(TRAIN{i}(1, :))));
        m = mean(TRAIN{i}(:,idx), 2);
        c = cov(TRAIN{i}(:,idx)', 1);
        scinv = chol(inv(c));
        % centrer et normer en taille les donn�es
        TRAIN{i}(:,idx) = scinv * (TRAIN{i}(:,idx) - repmat(m, 1, length(idx)));
        kmeansTrain = [kmeansTrain; TRAIN{i}(:,idx)']; %#ok<AGROW>
    end

    disp('Train Kmean Code ...');
% Choose one version
    %[IDX, Code] = Kmeans0(kmeansTrain, 256);
    %[IDX, Code] = Kmeans1(kmeansTrain, 256);
    [IDX, Code] = Kmeans2(kmeansTrain, 256);

    disp('compute CodeTrain...');
    CodeTrain = cell(length(TRAIN), 1);
    start = 1;
    for i = 1:length(TRAIN)
        CodeTrain{i} = zeros(size(TRAIN{i}, 2),1);
        ind = find(not(isnan(TRAIN{i}(1, :))));
        CodeTrain{i}(ind) = IDX (start:start + length(ind) - 1);
        start = start + length(ind);
        ind = find(isnan(TRAIN{i}(1, :)));
        ind2 = TRAIN{i}(2, ind) == 1; % PEN_UP
        CodeTrain{i}(ind(ind2)) = 257; % Code for PEN_UP
        ind2 = TRAIN{i}(2, ind) == -1; % PEN_DOWN
        CodeTrain{i}(ind(ind2)) = 258; % Code for PEN_DOWN
    end
end

function [CodeTest, LabelTest] = loadTestData(Code)
    disp('Load Test Data ...');
    [TEST, LabelTest] = loadUnipenData('pendigits-orig.tes');

    disp('Compute codetest ...');
    mdl=fitcknn(Code,1:256, 'NumNeighbors',1);
    CodeTest = cell(length(TEST), 1);
    for i = 1:length(TEST)
        CodeTest{i} = zeros(size(TEST{i}, 2), 1);
        ind = find(not(isnan(TEST{i}(1,:))));
        m = mean(TEST{i}(:,ind), 2);
        c = cov(TEST{i}(:,ind)', 1);
        scinv = chol(inv(c));
        % centrer et normer en taille les donn�es
        TEST{i}(:,ind) = scinv * (TEST{i}(:,ind) - repmat(m, 1, length(ind)));
        CodeTest{i}(ind) = predict(mdl, TEST{i}(:,ind)');
        ind = find(isnan(TEST{i}(1, :)));
        ind2 = TEST{i}(2, ind) == 1; % PEN_UP
        CodeTest{i}(ind(ind2)) = 257; % Code for PEN_UP
        ind2 = TEST{i}(2, ind) == -1; % PEN_DOWN
        CodeTest{i}(ind(ind2)) = 258; % Code for PEN_DOWN
    end
end


function M1 = split(M)
    [n, d] = size(M);
    M1 = zeros(2 * n, d);
    tmp = ones(n, d);
    tmp(rand(n,d) < .5) = -1;
    M1(1:n,:) = M + tmp * .05;
    M1(n+1:2*n, :) = M - tmp * .05;
end

function M1 = intelSplit(M, IDX, nbc)
    [n, d] = size(M);
    M1 = zeros(2 * n, d);
    card = hist(IDX, 1:nbc);
    M1(1:n, :) = M;
    for i=n+1:2*n
        [~, ind] = max(card);
        ind = max(ind); % En cas ou il y a plusieurs je prend le derniers
        tmp = ones(1,d); tmp(rand(1,d) < 0.5) = -1;
        M1(i, :) = M1(ind, :) + tmp * .05;
        card(ind) = card(ind) / 2;
        card(i) = card(ind);
    end
end
