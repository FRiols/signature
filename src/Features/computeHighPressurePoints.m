function pr = computeHighPressurePoints(x, y, pressure)
%% Compute the number of points with high pressure

points = [];
newX = [];
newY = [];
meanPressure = mean(pressure);

for pt = 1:length(pressure)
    if pressure(pt) > meanPressure
        points = [points pt];
        newX = [newX x(pt)];
        newY = [newY y(pt)];
    end
end

%figure;
%plot(x, y, 'b', 'LineWidth', 5);
%hold on;
%plot(newX, newY, 'ro', 'LineWidth', 5);
%pause;

pr = [newX', newY'];

end