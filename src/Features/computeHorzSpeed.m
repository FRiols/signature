function horzSpeed = computeHorzSpeed(x, time)

speed = zeros(length(x), 1);

for i = 2:length(x)
    
    speed(i) = 1000 * (x(i - 1) - x(i));
    dt = time(i) - time(i - 1);
    if (dt == 0)
        dt = 10;
    end
    speed(i) = speed(i) / dt;

end

horzSpeed = sum(speed);

end