function slowest = computeSlowPoints(x, y, time)

len = length(x);
sp = zeros(len, 2);

for i = 2:len - 1
    v2 = [x(i + 1) - x(i - 1); y(i) - y(i - 1)];
    dist = sqrt(v2(1) * v2(1) + v2(2) * v2(2));
    speed = dist / (time(i + 1) - time(i - 1));
    sp(i, 1) = speed;
    sp(i, 2) = i;
end


S = sortrows(sp, 1);

slowest = S(1:round(len/7), :);

slowest = sort(slowest(:, 2));

slowest = slowest(slowest ~= 0);

end