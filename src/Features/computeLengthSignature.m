function lengthSign = computeLengthSignature(x, y)

lengthSign = 0;

for i = 2:length(x)
    
    dist = sqrt((x(i - 1) - x(i))^2 + (y(i - 1) + y(i))^2);
    lengthSign = lengthSign + dist;
    
end

end