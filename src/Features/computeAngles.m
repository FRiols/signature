function angles = computeAngles(x, y)

len = length(x);
angles = zeros(len, 1);

for i = 2:len
    v = [x(i) - x(i - 1); y(i) - y(i - 1)];
    if (v(1) == 0 && v(2) == 0)
        angles(i) = 0;
    else
        cosTheta = v(1) / norm(v);
        angles(i) = acos(cosTheta);
    end
end

end