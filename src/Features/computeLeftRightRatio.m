function rgb = computeLeftRightRatio(x)

p1 = 0;
p2 = 0;

for i = 2:length(x)
    
    p1 = p1 + max(x(i - 1) - x(i), 0);
    p2 = p2 + min(x(i - 1) - x(i), 0);
    
end

rgb = -p1 / p2;

end