function apd = computeFirstLastAngles(x, y)

v1 = [1; 0];

v2 = [x(end) - x(1); y(end) - y(1)];
prodSc = sum(v1 .* v2);
v2Dist = sqrt(v2(1)^2 + v2(2)^2);
if (v2Dist == 0)
    apd = 0;
else
    cosTheta = prodSc / v2Dist;
    apd = acos(cosTheta);
end


end