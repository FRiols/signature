function listTest = test(file)

%% LOAD FILE

load('TRAIN.mat');


%% LOAD TEST FILES LIST

% Open the file
fileID = fopen(file, 'r');
numLine = 0;

% Set the number of points in the signature
i = 1;
t = cell(1, 1);

while true
    err = fgetl(fileID);
    
    if ~ischar(err)
        break
    end
    
    t{i} = strsplit(err);
    i = i + 1;
    numLine = numLine + 1;
end

t = t';
listTest = cell(size(t));

for i = 1:size(t, 1)
    
    testFile = t{i}(1);
    versusID = t{i}(2);
    listTest{i}.testFile = testFile;
    listTest{i}.versusID = versusID;
    
end

fclose('all');


%% PARAMETERS
logNumClasses = 4;
sizeTrEmit = 6;

scorePourc = zeros(3, 1);
fn = zeros(3, 1);
fp = zeros(3, 1);
size136 = zeros(3, 1);


%% READ FILE

fileID1 = fopen('score1', 'w');
fileID3 = fopen('score3', 'w');
fileID6 = fopen('score6', 'w');

for i = 1:numLine
    if mod(i, 100) == 0
        i
    end
    %% LOAD FILE
    data = loadfile(strcat('../../sample/', char(listTest{i}.testFile)));
    TestVector = [data.x data.y data.azimuth]';
    listTest{i}.testVector = TestVector;
    
    %% EXTRACT ID
    for j = 1:size(listTrain, 1)
        if char(listTrain{j}.id) == char(listTest{i}.versusID)
            idListTrain = j;
            break
        end
    end

    
    %% FIT TO KMEANS
    listTest{i}.mdl = fitcknn(listTrain{idListTrain}.code, ...
        1:2^logNumClasses, 'NumNeighbors', 1);

    
    %% NORMALIZE DATA

    m = mean(TestVector, 2); % Moyenne des coordonnées. Vecteurs 2x1
    C = cov(TestVector', 1); % Covariance des coordonnées. Vecteurs 2x1
    CinvzT = chol(inv(C)); % Cholesky de l'inverse de la cov. Vecteurs 2x1
    
    % Centrer et normer en taille les données
    TEST = CinvzT * ...
        (TestVector - repmat(m, 1, size(TestVector, 2)));
    CodeTest = predict(listTest{i}.mdl, TEST');
    
    ind = find(TEST(1, :) == nan);
    ind2 = find(TEST(2, ind) == 1); % PEN_UP
    CodeTest(ind(ind2)) = 2^logNumClasses + 1; % Code for PEN_UP
    ind2 = find(TEST(2, ind) == -1); % PEN_DOWN
    CodeTest(ind(ind2)) = 2^logNumClasses + 2; % Code for PEN_DOWN


   %% COMPUTE SCORE (DTW FOR 1)
    if size(listTrain{idListTrain}.trainFiles, 1) == 1
        numTrain = 1;
        listTest{i}.score = 0;
        continue;
    elseif size(listTrain{idListTrain}.trainFiles, 1) == 3
        numTrain = 2;
    elseif size(listTrain{idListTrain}.trainFiles, 1) == 6
        numTrain = 3;
        listTest{i}.score = 0;
        continue;
    end
    
    size136(numTrain) = size136(numTrain) + 1;
    
    char(listTrain{idListTrain}.id);
    char(listTest{i}.testFile);
    
    if numTrain == 1 && listTrain{idListTrain}.ready == true
        testF = char(listTest{i}.testFile);
        vs = char(listTest{i}.versusID);
        score = computeDTW(listTrain{idListTrain}.train{1, 1}, TEST);
        listTest{i}.score = score;
    else
        [pstates, score] = hmmdecode(CodeTest', ...
            listTrain{idListTrain}.est_tr, listTrain{idListTrain}.est_emit);
        listTrain{idListTrain}.id;
        listTest{i}.testFile;
        listTest{i}.score = score;
    end
    
    
    if isnan(listTest{i}.score)
        listTest{i}.score = -inf;
    end
    
    %% TAKE DECISION
    if listTrain{idListTrain}.ready == false
        continue;
    end
    
    if numTrain ~= 1 && (listTest{i}.score < listTrain{idListTrain}.threshold ...
            || isnan(listTest{i}.score))
        listTest{i}.impostor = 'f';
    elseif numTrain == 1 && listTest{i}.score > listTrain{idListTrain}.threshold
        listTest{i}.impostor = 'f';
    else
        listTest{i}.impostor = 't';
    end
    
    th = listTrain{idListTrain}.threshold;
    imp = listTest{i}.impostor;
    
    %% CHECK IF RIGHT
    a = char(listTest{i}.testFile);
    b = char(listTest{i}.versusID);
    
    if listTest{i}.impostor == 'f'
        if strcmp(a(1:5), b(1:5)) == 0
            scorePourc(numTrain) = scorePourc(numTrain) + 1;
        else
            fn(numTrain) = fn(numTrain) + 1;
        end
    else
        if strcmp(a(1:5), b(1:5)) == 1
            scorePourc(numTrain) = scorePourc(numTrain) + 1;
        else
            fp(numTrain) = fp(numTrain) + 1;
        end
    end
    
    %% WRITE INTO FILE   
    surete = listTest{i}.score - listTrain{idListTrain}.threshold;
        
    if size(strfind(char(listTest{i}.versusID), '_1'), 1) ~= 0
        fid = fileID1;
    elseif size(strfind(char(listTest{i}.versusID), '_3'), 1) ~= 0
        fid = fileID3;
    else
        fid = fileID6;
    end
    
    format long
    fprintf(fid, '%s %s %.0f %s\n', char(listTest{i}.testFile), ...
        char(listTest{i}.versusID), surete, listTest{i}.impostor);
end

size136
fp = fp * 100 ./ size136
fn = fn * 100 ./ size136
scorePourc = scorePourc * 100 ./ size136


fclose('all');

%% SAVE
save

end