function DATA_KMEANS = computeTrainCode(TRAIN, logNumClasses, idx)

DATA_KMEANS = cell(length(TRAIN), 1);
start = 1;
for i = 1:length(TRAIN)
    DATA_KMEANS{i} = zeros(size(TRAIN{i}, 2), 1);
    
    ind = find(TRAIN{i}(1, :) ~= nan);
    DATA_KMEANS{i}(ind) = idx(start:start + length(ind) - 1);
    
    start = start + length(ind);
    ind = find(TRAIN{i}(1, :) == nan);

    ind2 = find(TRAIN{i}(2, ind) == 1); % PEN_UP
    DATA_KMEANS{i}(ind(ind2)) = 2^logNumClasses + 1; % Code for PEN_UP

    ind2 = find(TRAIN{i}(2, ind) == -1); % PEN_DOWN
    DATA_KMEANS{i}(ind(ind2)) = 2^logNumClasses + 2; % Code for PEN_DOWN
end

end