function [idx code] = runKMeans(DATA_KMEANS, logNumClasses)

code = mean(DATA_KMEANS); % Vecteur 1x2
nbclasses = 1;
N = size(DATA_KMEANS, 1); % ~300000

% Initialisation et run intelligents pour le k-means
for i = 1:logNumClasses
    t = rand(N, 1);
    ind = find(t <= 1);
    nbclasses = nbclasses * 2;
    newa = split(code);
    if i == logNumClasses
        [idx code] = kmeans(DATA_KMEANS, nbclasses, 'start', newa, ...
                'MaxIter', 150);
    else
        [idx code] = kmeans(DATA_KMEANS(ind, :), nbclasses, 'start', newa, ...
                'MaxIter', 150);
    end
end

end
