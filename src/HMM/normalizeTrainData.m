function [TRAIN DATA_KMEANS] = normalizeTrainData(TrainVector)

TRAIN = cell(size(TrainVector));
DATA_KMEANS = [];

for i = 1:length(TrainVector)
    
    m = mean(TrainVector{i}, 2); % Moyenne des coordonnées. Vecteurs 2x1
    C = cov(TrainVector{i}', 1); % Covariance des coordonnées. Vecteurs 2x1
    Cinvz = chol(inv(C));
    % Cholesky de l'inverse de la cov. Vecteurs 2x1
    
    % Centrer et normer en taille les données
    TRAIN{i} = Cinvz * (TrainVector{i} - repmat(m, 1, size(TrainVector{i}, 2)));
    DATA_KMEANS = [DATA_KMEANS; TRAIN{i}']; % Tous les coordonnées normaliser. ~350x2
    
end


end