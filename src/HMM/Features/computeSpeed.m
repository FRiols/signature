function speed = computeSpeed(data)

speed = zeros(length(data.x), 1);

for i = 2:length(data.x)
    speed(i) = (1000 * (data.x(i - 1) - data.x(i)))^2 + ...
        (1000 * (data.y(i - 1) - data.y(i)))^2;
    dt = data.time(i) - data.time(i - 1);
    if (dt == 0)
        dt = 10;
    end
    speed(i) = sqrt(speed(i)) / dt;
end


end