function train(file)

%% LOAD TRAIN FILES LIST

% Open the file
fileID = fopen(file, 'r');
numLine = 0;

% Set the number of points in the signature
i = 1;
t = cell(1, 1);

while true
    err = fgetl(fileID);
    
    if ~ischar(err)
        break
    end
    
    t{i} = strsplit(err);
    i = i + 1;
    numLine = numLine + 1;
end

t = t';
listTrain = cell(size(t));

for i = 1:size(t, 1)
    
    id = t{i}(1);
    trainFiles = t{i}(2:end);
    listTrain{i}.id = id;
    listTrain{i}.trainFiles = trainFiles';
    
end

fclose('all');


%% PARAMETERS
logNumClasses = 4;
sizeTrEmit = 6;


%% TRAIN DATA

for i = 1:numLine
    
    %% LOAD FILE
    TrainVector = cell(1, size(listTrain{i}.trainFiles, 1));

    for j = 1:size(listTrain{i}.trainFiles, 1)
        data = loadfile(strcat('../../sample/', char(listTrain{i}.trainFiles(j))));
        TrainVector{1, j} = [data.x data.y data.azimuth]';
    end
    
    listTrain{i}.trainVector = TrainVector';
    
    
    %% NORMALISE DATA
    
    [TRAIN, DATA_KMEANS] = normalizeTrainData(TrainVector);
    listTrain{i}.train = TRAIN;
    listTrain{i}.Data_Kmeans = DATA_KMEANS;
    
    
    %% RUN K-MEANS

    [idx, code] = runKMeans(DATA_KMEANS, logNumClasses);
    listTrain{i}.idx = idx;
    listTrain{i}.code = code;
    
    %% CREATE STATES
    
    DATA_KMEANS = computeTrainCode(TRAIN, logNumClasses, idx);
    listTrain{i}.Data_Kmeans = DATA_KMEANS;


    %% RUN HMM
    
    [EST_tr, EST_emit] = trainHMM(DATA_KMEANS, logNumClasses, sizeTrEmit);
    listTrain{i}.est_tr = EST_tr;
    listTrain{i}.est_emit = EST_emit;
    listTrain{i}.ready = false;
    
end


%% SAVE DATA

save('TRAIN.mat', 'listTrain');

%% FIND THRESHOLD

fid = fopen('thresholdSearch.lst', 'w');
for i = 1:numLine
    for j = 1:size(listTrain{i}.trainFiles, 1)
        
        fprintf(fid, '%s %s\n', char(listTrain{i}.trainFiles(j)), ...
            char(listTrain{i}.id));
        
    end
end


listTest = test('thresholdSearch.lst');

%maxLine = size(listTrain{i}.trainFiles, 1) * 1:size(listTrain, 1)

it = 1;
for i = 1:size(listTrain, 1)
    threshold = [];
    for j = 1:size(listTrain{i}.trainFiles, 1)
        threshold = [threshold, listTest{it}.score];
        it = it + 1;
    end
    
    listTrain{i}.id
    MIN = min(threshold);
    MOY = mean(threshold)
    delta = std(threshold)

    if delta == 0
        thresholdValue = 8 * MOY
    elseif delta < 10
        thresholdValue = MOY - delta * 25
    elseif delta < 20
        thresholdValue = MOY - delta * 20
    elseif delta < 40
        thresholdValue = MOY - delta * 5
    else
        thresholdValue = MOY - delta
    end

    if size(listTrain{i}.trainFiles, 1) == 1
        thresholdValue = 158;
    end
    
    listTrain{i}.threshold = thresholdValue;
    listTrain{i}.ready = true;

end
    
save('TRAIN.mat', 'listTrain'); 

end