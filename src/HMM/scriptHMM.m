function [ScorePourc fn fp] = scriptHMM(userID)

close all;
addpath('Features');

%% PARAMETERS
logNumClasses = 4; % 2^6 = 64 classes
sizeTrEmit = 6;

%% Load data
%disp('Loading data...');

user = '../../sample/USER';
%trainImages = [randi([1 20], 1, 1)];
%trainImages = [randi([1 7], 1, 1) randi([8 15], 1, 1) randi([16 20], 1, 1)];
%trainImages = [randi([1 4], 1, 1) randi([5 8], 1, 1) randi([9 12], 1, 1) ...
%    randi([13 15], 1, 1) randi([15 17], 1, 1) randi([17 20], 1, 1)];
trainImages = [1 2 3];
testImages = [4];
label = [1];

% for i = 1:40
%     if sum(ismember(trainImages, i)) == 0
%         testImages = [testImages i];
%         if i <= 20
%             label = [label 1];
%         else
%             label = [label 0];
%         end
%     end
% end

[TrainVector dv] = initTrainData(trainImages, user, userID);


%% Normalisation
%disp('Normalize TRAIN data...');

[TRAIN DATA_KMEANS] = normalizeTrainData(TrainVector);


%% Initialize DATA_KMEANS et K-MEANS
%disp('K-means...');

[idx code] = runKMeans(DATA_KMEANS, logNumClasses);


%% Affichage

%displayKmeansClasses(false, dv, DATA_KMEANS, code);

%% Create states
%disp('compute DATA_KMEANS...');

DATA_KMEANS = computeTrainCode(TRAIN, logNumClasses, idx);


%% Train HMM
%disp('Train HMM...');

[EST_tr EST_emit] = trainHMM(DATA_KMEANS, logNumClasses, sizeTrEmit);


%% Find threshold
scoreTrain = testHMM(trainImages, user, userID, logNumClasses, code, EST_tr, ...
    EST_emit, false)

MIN = min(scoreTrain);
MOY = mean(scoreTrain);
delta = std(scoreTrain)

if delta < 20
    threshold = MOY - delta * 10
elseif delta < 40
    threshold = MOY - delta * 5
else
    threshold = MOY - delta
end


%% Test HMM
plotTest = true;
score = testHMM(testImages, user, userID, logNumClasses, code, EST_tr, ...
    EST_emit, plotTest);


%% Pourcentage of error

ScorePourc = 0;
fn = 0;
fp = 0;

score

for i = 1:size(testImages, 2)
    if label(i) == 1 && score(i) >= threshold;
        ScorePourc = ScorePourc + 1;
    elseif label(i) == 0 && (score(i) < threshold || isnan(score(i)))
        ScorePourc = ScorePourc + 1;
    elseif label(i) == 1 && (score(i) < threshold || isnan(score(i)))
        fn = fn + 1;
    elseif label(i) == 0 && score(i) >= threshold
        fp = fp + 1;
    end
    if i == 14
        sprintf('\n');
    end
end

ScorePourc = ScorePourc * 100 / size(testImages, 2);
fn = fn * 100 / size(testImages, 2);
fp = fp * 100 / size(testImages, 2);

fclose('all');

end
