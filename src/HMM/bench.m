function [scoreMeanTot, scoreMinTot, fnMeanTot, fpMeanTot] = bench()

numIt = 30;

scoreMinTot = zeros(1, 5);
scoreMeanTot = zeros(1, 5);
fnMeanTot = zeros(1, 5);
fpMeanTot = zeros(1, 5);

%% ------------------------------------ ICI ------------------------------------
for user = 1:5
    scoreMin = 100;
    scoreMean = 0;
    fnMean = 0;
    fpMean = 0;
    
    for i = 1:numIt
        i
        [ScorePourc, fn, fp] = scriptHMM(user);
        scoreMin = min(scoreMin, ScorePourc);
        scoreMean = scoreMean + ScorePourc;
        fnMean = fnMean + fn;
        fpMean = fpMean + fp;
    end
    
    scoreMinTot(user) = scoreMin;
    scoreMeanTot(user) = scoreMean / numIt;
    fnMeanTot(user) = fnMean / numIt;
    fpMeanTot(user) = fpMean / numIt;
    [scoreMeanTot; scoreMinTot; fnMeanTot; fpMeanTot]

end

end
