function score = testHMM(testImages, user, userID, logNumClasses, code, ...
    EST_tr, EST_emit, plotTest)

% Store the coordinates (x, y), time, status, azimuth, altitude and
% pressure
TestVector = cell(1, size(testImages, 2));

% Load and normalize data
for n = 1:size(testImages, 2)
    
    data = loadfile(strcat(user, int2str(userID), '_', int2str(testImages(n)), '.txt'));

    data.speed = computeSpeed(data);
    acc = computeAcceleration(data.speed, data.time);
    angles = computeAngles(data.x, data.y);
    %hSpeed = computeHorzSpeed(data.x, data.time);
    
    %TestVector{1, n} = [data.x data.y data.azimuth acc angles data.speed]';
    TestVector{1, n} = [data.x data.y data.azimuth]';
    %TestVector{1, n} = [data.x data.y data.pressure data.azimuth data.altitude]';
        
    data2 = normalizeData(data);
    dv{n, 1} = data2;
    
end

if plotTest == true
    plotMultiple(dv, size(testImages, 2));
end

mdl = fitcknn(code, 1:2^logNumClasses, 'NumNeighbors', 1);
TEST = cell(size(TestVector));
CodeTest = cell(size(TEST));

%fprintf('Normalize TEST data...\n');

for i = 1:size(testImages, 2)

    m = mean(TestVector{i}, 2); % Moyenne des coordonnées. Vecteurs 2x1
    C = cov(TestVector{i}', 1); % Covariance des coordonnées. Vecteurs 2x1
    CinvzT = chol(inv(C)); % Cholesky de l'inverse de la cov. Vecteurs 2x1
    
    % Centrer et normer en taille les données
    TEST{i} = CinvzT * (TestVector{i} - repmat(m, 1, size(TestVector{i}, 2)));
    CodeTest{i} = predict(mdl, TEST{i}');
    
    ind = find(TEST{i}(1, :) == nan);
    ind2 = find(TEST{i}(2, ind) == 1); % PEN_UP
    CodeTest{i}(ind(ind2)) = 2^logNumClasses + 1; % Code for PEN_UP
    ind2 = find(TEST{i}(2, ind) == -1); % PEN_DOWN
    CodeTest{i}(ind(ind2)) = 2^logNumClasses + 2; % Code for PEN_DOWN 

end

score = [];

for i = 1:size(testImages, 2)
    [pstates, score_i] = hmmdecode(CodeTest{i}', EST_tr, EST_emit);
    score = [score; score_i];
end

end
