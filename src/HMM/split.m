function b = split(a)

    [n d] = size(a);
    b = zeros(2 * n, d);
    tmp = rand(size(a));
    fact = ones(size(a));
    fact(tmp < 0.5) = -1;
    b(n + 1:2*n, 1:d) = a + fact * 0.01;
    b(1:n, 1:d) = a - fact * 0.01;
    
end