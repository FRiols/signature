function DATA = loadfile(filename)
%% Load the file

% Open the file
fileID = fopen(filename, 'r');

% Set the number of points in the signature
numPoints = str2num(fgets(fileID));

% Set the number of parameters
numParameters = 7;

%% Create the structure

X = zeros(numPoints, 1);
Y = zeros(numPoints, 1);
Time = zeros(numPoints, 1);
Status = zeros(numPoints, 1);
Azimuth = zeros(numPoints, 1);
Altitude = zeros(numPoints, 1);
Pressure = zeros(numPoints, 1);

for i = 2:numPoints + 1
    line = fgets(fileID);
    line = str2num(line);
    
    X(i - 1) = line(1);
    Y(i - 1) = line(2);
    Time(i - 1) = line(3);
    Status(i - 1) = line(4);
    Azimuth(i - 1) = line(5);
    Altitude(i - 1) = line(6);
    Pressure(i - 1) = line(7);
end

DATA.x = X;
DATA.y = Y;
DATA.time = Time;
DATA.status = Status;
DATA.azimuth = Azimuth;
DATA.altitude = Altitude;
DATA.pressure = Pressure;

end