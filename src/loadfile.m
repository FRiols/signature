function DATA = loadfile(fileID)
%% Load the file

% Transform the file into data
array = fscanf(fileID, '%d');
numPoints = array(1); % Set the number of points in the signature

% Set the number of parameters
numParameters = 7;

%% Create the structure
X = zeros(numPoints, 1);
Y = zeros(numPoints, 1);
Time = zeros(numPoints, 1);
Status = zeros(numPoints, 1);
Azimuth = zeros(numPoints, 1);
Altitude = zeros(numPoints, 1);
Pressure = zeros(numPoints, 1);

pos = 2;    % The start of the actual data
for i = 2:numPoints + 1
    X(i - 1) = array(pos);
    Y(i - 1) = array(pos + 1);
    Time(i - 1) = array(pos + 2);
    Status(i - 1) = array(pos + 3);
    Azimuth(i - 1) = array(pos + 4);
    Altitude(i - 1) = array(pos + 5);
    Pressure(i - 1) = array(pos + 6);
    pos = pos + 7;
end

DATA.x = X;
DATA.y = Y;
DATA.time = Time;
DATA.status = Status;
DATA.azimuth = Azimuth;
DATA.altitude = Altitude;
DATA.pressure = Pressure;

fclose(fileID);

end