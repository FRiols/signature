function listTest = test(file, fileScore, isTraining, testNumber)
%% Test with the training data
% file: the list of tests
% fileScore: the output file
% isTraining (optional, omit except during training)

addpath('Features');

if nargin < 3
    isTraining = false;
end

tic
%% LOAD TRAIN DATA

load('TRAIN.mat', 'listTrain');

% Needed for parfor
listTrain = listTrain; %#ok<ASGSL,NODEF>
eucScoreMean = 0;
trueScoreMean = 0;
count1 = 0;
count2 = 0;


%% LOAD TEST FILES LIST

[numLine, listTest] = readTestFile(file);

%% PARAMETERS

logNumClasses = 4; % for KMeans

fn = zeros(3, 1); % false alarms
fp = zeros(3, 1); % false positive
display = zeros(2, numLine); % graph data


%% READ FILE

fileID = fopen(fileScore, 'w');

%% Main Loop, evaluate the data (DTW)
ITER_STEP = 100;

% split the data in batches of ITER_STEP, to have a parfor but still write
% as we compute (at the end of every batch)
for j = 0:idivide(int32(length(listTest) - 1), ITER_STEP)+1
    iter_begin = 1+(ITER_STEP*j);
    iter_end = min(ITER_STEP*(j + 1), length(listTest));

    parfor i = iter_begin:iter_end

      %% LOAD FILE
      data = loadfile(strcat('../../sample/', char(listTest{i}.testFile)));
      speed = computeSpeed(data.x, data.y, data.time);
      acc = computeAcceleration(speed, data.time);
      [minimumSpeedX, minimumSpeedY] = computeMinSpeed(data, speed);
      feat = computeFeatures(data, speed);
      
      %TestVectorDTW = [data.y data.x]';
      TestVectorDTW = GetDTWVector(data, speed, acc, testNumber, false, minimumSpeedX, minimumSpeedY)';
      listTest{i}.testVectorDTW = TestVectorDTW;
      listTest{i}.featuresVector = feat / max(feat);
      
      %% EXTRACT ID

      idListTrain = extractID(listTest{i}, listTrain);

      listTest{i}.idListTrain = idListTrain;

      %% FIT TO KMEANS

%       listTest{i}.mdl_DTW = fitcknn(listTrain{idListTrain}.code_DTW, ...
%           1:2^logNumClasses, 'NumNeighbors', 1);


      %% NORMALIZE DATA

      [TEST_DTW, ~] = normalizeTestData(TestVectorDTW, 0, ...
          logNumClasses);

      % 1 -> 1, 3 -> 2, 6 -> 3
      numTrain = idivide(int8(size(listTrain{idListTrain}.trainFiles, 1)), 3) + 1;


      %% DTW
      scoreDTW = 0;
      for k = 1:numTrain
          scoreDTW = scoreDTW + computeDTW(listTrain{idListTrain}.train_DTW{1, k}, TEST_DTW);
      end

      scoreDTW = scoreDTW / double(numTrain);
      listTest{i}.scoreDTW = scoreDTW;
      listTest{i}.scoreEuclideanFeatures = dist(listTest{i}.featuresVector, listTrain{idListTrain}.featuresVector');
      if (listTest{i}.scoreEuclideanFeatures > 0.35)
        listTest{i}.scoreDTW = listTest{i}.scoreDTW + 1000;    
      end
      % If we're training, there's no need to take the decision
      if isTraining
          continue;
      end
      
      %% TAKE DECISION

      [imp, scDTW, thr] = takeDecision(listTrain{idListTrain}, listTest{i});
      listTest{i}.impostor = imp;
      if imp == 1
          eucScoreMean = eucScoreMean + listTest{i}.scoreEuclideanFeatures;
      else
          trueScoreMean = trueScoreMean + listTest{i}.scoreEuclideanFeatures;
      end
      
      listTest{i}.score = scDTW;
      listTest{i}.threshold = thr;

      %% CHECK IF RIGHT

      [dDTW, fn_, fp_] = computeSuccess(listTest{i});

      display(:, i) = dDTW;
      fn = fn + fn_;
      fp = fp + fp_;

    end

    fprintf('Iteration number: %i/%i\n', iter_end, numLine);

    %% WRITE INTO FILE
    if not(isTraining)
        for i = iter_begin:iter_end
          writeScoreFile(listTest{i}, fileID);
        end
    end
    
end

toc % measure running time

%% Output statistics

%surePositive = [76; 68; 56];
%sureNegative = [704; 704; 704];

fprintf('False positive: %i\n', fp(1));
fprintf('False negative: %i\n', fn(1));
%fp = fp * 100 ./ sureNegative
%fn = fn * 100 ./ surePositive

%eucScoreMean/numLine
%trueScoreMean/numLine


% Thresholds for each user
fprintf('Thresholds\n');
for i = 1 : size(listTrain, 1)
    th = listTrain{i}.thresholdDTW1;
    fprintf('%s: %d\n', listTrain{i}.id{1}, floor(th));
end

%% Draw graph
% Display a graph of the sample
% top is better (likely to match)
% In blue are matching samples, in red non-matching
% The green line is our decision threshold
if not(isTraining)
    figure(1);
    clf;
    hold on;
    plot([1 numLine], [-global_thrsh(), -global_thrsh()], 'g');
    good = display(1, :)' ~= 0;
    idx = 1:numLine;
    plot(idx(good), display(1, good), 'or', 'LineWidth', 0.1);
    plot(idx(~good), display(2, ~good), 'xb', 'LineWidth', 5);
end

%% Save data
% save('testSaved.mat', 'listTest', 'listTrain', 'numLine');

end
