function vectorDTW = GetDTWVector(data, speed, acc, num, first)
    vectorDTW = [];
    if first == true, fprintf('%d: [ ', num); end
    if (num < 7)
        vectorDTW = [GetCorrectDTWDATA(data, speed, acc, num, first)];
    elseif (num < 49 + 7)
        vectorDTW = [GetCorrectDTWDATA(data, speed, acc, mod((num - 7), 7), first) ...
                     GetCorrectDTWDATA(data, speed, acc, floor((num - 7) / 7), first)];
    elseif (num < 343 + 49 + 7)
        vectorDTW = [GetCorrectDTWDATA(data, speed, acc, mod((num - 49 - 7), 7), first) ...
                     GetCorrectDTWDATA(data, speed, acc, mod(floor((num - 49 - 7) / 7), 7), first) ...
                     GetCorrectDTWDATA(data, speed, acc, floor((num - 49 - 7) / 49), first)];
    elseif (num < 2401 + 343 + 49 + 7)
        vectorDTW = [GetCorrectDTWDATA(data, speed, acc, mod((num - 343 - 49 - 7), 7), first) ...
                     GetCorrectDTWDATA(data, speed, acc, mod(floor((num - 343 - 49 - 7) / 7), 7), first) ...
                     GetCorrectDTWDATA(data, speed, acc, mod(floor((num - 343 - 49 - 7) / 49), 7), first) ...
                     GetCorrectDTWDATA(data, speed, acc, floor((num - 343 - 49 - 7) / 343), first)];
    end
    if first == true, disp(']'); end

end

function vectorDTWData = GetCorrectDTWDATA(data, speed, acc, num, first)
   if (num == 0)
       vectorDTWData = [data.x];
       if first == true, fprintf('x '); end
   elseif (num == 1)
       vectorDTWData = [data.y];
       if first == true, fprintf('y '); end
   elseif (num == 2)
       vectorDTWData = [data.azimuth];
       if first == true, fprintf('azimuth '); end
   elseif (num == 3)
       vectorDTWData = [data.altitude];
       if first == true, fprintf('altitude '); end
   elseif (num == 4)
       vectorDTWData = [data.pressure];
       if first == true, fprintf('pressure '); end
   elseif (num == 5)
       vectorDTWData = [speed];
       if first == true, fprintf('speed '); end
   else
       vectorDTWData = [acc];
       if first == true, fprintf('acc '); end
   end
end