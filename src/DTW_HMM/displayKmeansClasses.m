function displayKmeansClasses(displayTrain, dv, DATA_KMEANS, code)

if displayTrain == true
    plotMultiple(dv, size(dv, 1));
end

figure(2);
plot(DATA_KMEANS(:, 1), DATA_KMEANS(:, 2), 'ob');
hold on;
plot(code(:, 1), code(:, 2), 'xr', 'LineWidth', 5);
hold off;

end