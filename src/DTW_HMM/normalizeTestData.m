function [TEST, CodeTest] = normalizeTestData(TestVector, mdl, ...
    logNumClasses)

    m = mean(TestVector, 2); % Moyenne des coordonnées. Vecteurs 2x1
    C = cov(TestVector', 1); % Covariance des coordonnées. Vecteurs 2x1
    CinvzT = chol(inv(C)); % Cholesky de l'inverse de la cov. Vecteurs 2x1

    % Centrer et normer en taille les données
    TEST = CinvzT * (TestVector - repmat(m, 1, size(TestVector, 2)));
    CodeTest = [];
%     CodeTest = predict(mdl, TEST');
% 
%     ind = find(TEST(1, :) == nan);
%     ind2 = find(TEST(2, ind) == 1); % PEN_UP
%     CodeTest(ind(ind2)) = 2^logNumClasses + 1; % Code for PEN_UP
%     ind2 = find(TEST(2, ind) == -1); % PEN_DOWN
%     CodeTest(ind(ind2)) = 2^logNumClasses + 2; % Code for PEN_DOWN

end
