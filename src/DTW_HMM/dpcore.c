/*
 *  dpcore.c
 *  Core of dynamic programming/DTW calculation
 * 2003-04-02 dpwe@ee.columbia.edu
 * $Header: /Users/dpwe/projects/dtw/RCS/dpcore.c,v 1.5 2014/06/20 21:08:53 dpwe Exp dpwe $
 % Copyright (c) 2003-05 Dan Ellis <dpwe@ee.columbia.edu>
 % released under GPL - see file COPYRIGHT
 */

#include    <stdio.h>
#include    <float.h>  /* per Vigi Katlowitz for windows 2014-06-20 */
#include    <math.h>
#include    <ctype.h>

/* these two lines are to make it compile on Xcode 5.1 2014-06-20 */
#include <stdint.h>
typedef uint16_t char16_t;

#include    "mex.h"

/* #define INF HUGE_VAL */
#define INF DBL_MAX

static inline double min(double a, double b)
{
  return a < b ? a : b;
}

void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  if (nrhs < 1)
  {
    mexPrintf("dpcore  [D,P] = dpcore(S[,C])  dynamic programming core\n");
    mexPrintf("           Calculate the best cost to every point in score\n");
    mexPrintf("           cost matrix S; return it in D along with traceback\n");
    mexPrintf("           indices in P. Optional C defines allowable steps\n");
    mexPrintf("           and costs; default [1 1 1.0;1 0 1.0;0 1 1.0]\n");
    return;
  }


  int rows = mxGetM(prhs[0]);
  int cols = mxGetN(prhs[0]);
  double *pM = mxGetPr(prhs[0]);

  double *pD = malloc(rows * sizeof (double));

  /* setup costs */

  /* default C matrix */

  /* do dp */

  // First row
  pD[0] = pM[0];
  for (int i = 1; i < rows; ++i)
    pD[i] = pD[i-1] + pM[i];

  for (int j = 1; j < cols; ++j)
  {
    for (int i = rows - 1; i > 0; --i)
      pD[i] = min(pD[i], pD[i - 1]);
    for (int i = rows - 1; i > 0; --i)
      pD[i] = min(pD[i], pD[i - 1]) + pM[i + j * rows];
    if (j)
      pD[0] += pM[j * rows];
  }

  plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
  *mxGetPr(plhs[0]) = pD[rows - 1];
  free(pD);

}

