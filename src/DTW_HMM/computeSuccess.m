function [display, fn, fp] = computeSuccess(testEl)

    a = char(testEl.testFile);
    b = char(testEl.versusID);
    falseExDTW = 0;
    trueExDTW = 0;

    fn = 0;
    fp = 0;
    if testEl.impostor
        if a(5) == b(5) && str2double(a(7:8)) <= 20
            fn = 1;
        end
    else
        if a(5) ~= b(5) || str2double(a(7:8)) > 20
            fp = 1;
        end
    end

    if a(5) == b(5) && str2double(a(7:8)) <= 20
        trueExDTW = testEl.score;
    else
       % testEl.scoreEuclideanFeatures
        falseExDTW = testEl.score;
    end

    display = [falseExDTW; trueExDTW];
    

end
