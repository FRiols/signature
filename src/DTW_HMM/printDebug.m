function printDebug(listTrain, idListTrain, listTest, i)

testFile = listTest{i}.testFile;
versusID = listTest{i}.versusID;
fprintf('%s\n%s.txt\n', char(testFile), char(versusID));

surete = listTest{i}.score - listTest{i}.threshold;
sureteDTW = listTrain{idListTrain}.thresholdDTW - listTest{i}.scoreDTW;
sureteHMM_fp = listTest{i}.scoreHMM - listTrain{idListTrain}.thresholdHMM_fp;
sureteHMM_fn = listTest{i}.scoreHMM - listTrain{idListTrain}.thresholdHMM_fn;

if sureteHMM_fp < 0 || sureteHMM_fn > 0
    decisionHMM = 'f';
else
    decisionHMM = 't';
end

if sureteDTW < 0
    decisionDTW = 'f';
else
    decisionDTW = 't';
end

format long
fprintf('    SCORE: DTW: %0.0f\t HMM: %0.0f\t\t FINAL: %0.0f\n', ...
    listTest{i}.scoreDTW, ...
    listTest{i}.scoreHMM, ...
    listTest{i}.score);

fprintf('THRESHOLD: DTW: %0.0f\t HMM: %0.0f\t %0.0f\t FINAL: %0.0f\n', ...
    listTrain{idListTrain}.thresholdDTW, ...
    listTrain{idListTrain}.thresholdHMM_fp, ...
    listTrain{idListTrain}.thresholdHMM_fn, ...
    listTest{i}.threshold);

fprintf('   SURETE: DTW: %0.0f\t HMM: %0.0f\t %0.0f\t FINAL: %0.0f\n', ...
    sureteDTW, ...
    sureteHMM_fp, ...
    sureteHMM_fn, ...
    surete);

decision = 't';
if listTest{i}.impostor
    decision = 'f';
end
fprintf(' DECISION: DTW: %s\t HMM: %s\t\t\t FINAL: %s\n\n', ...
    decisionDTW, ...
    decisionHMM, ...
    decision);

pause;


end