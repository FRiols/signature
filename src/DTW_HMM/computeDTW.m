function score = computeDTW(a, b)
%% Compute the DTW distance between a and b

    dim = size(a, 1);
    assert(dim == size(b, 1));

    ind = isnan(a);
    a(ind) = 0;

    ind = isnan(b);
    b(ind) = 0;

    n = length(a);
    m = length(b);

    if n > 2 * m || m > 2 * n
        score = Inf;
        return;
    end;

    costs = zeros(n, m);
    for i = 1:dim
      costs = costs + (repmat(a(i, :)', 1, m) - repmat(b(i, :), n, 1)) .^ 2;
    end

    costs = sqrt(costs);

    score = dpcore(costs);
end
