function [impostor, score, threshold] = takeDecision(trainEl, testEl)
%% normalize the distance by the threshold and take the decision

  threshold = trainEl.thresholdDTW1;
  score = 1000 * testEl.scoreDTW / threshold;
  impostor = score >= global_thrsh() || (testEl.scoreEuclideanFeatures > 0.35);
  if score == Inf
      score = 3000.0;
  end
  score = -score;
end
