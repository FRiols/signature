function idListTrain = extractID(testEl, listTrain)

    for j = 1:size(listTrain, 1)
        if char(listTrain{j}.id) == char(testEl.versusID)
            idListTrain = j;
            break
        end
    end

end
