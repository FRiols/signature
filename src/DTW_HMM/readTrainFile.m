function [numLine, listTrain] = readTrainFile(file)

% Open the file
fileID = fopen(file, 'r');
numLine = 0;

% Set the number of points in the signature
i = 1;
t = cell(1, 1);

while true
    err = fgetl(fileID);
    
    if ~ischar(err)
        break
    end
    
    t{i} = strsplit(err);
    i = i + 1;
    numLine = numLine + 1;
end

t = t';
listTrain = cell(size(t));

for i = 1:size(t, 1)
    
    id = t{i}(1);
    trainFiles = t{i}(2:end);
    listTrain{i}.id = id;
    listTrain{i}.trainFiles = trainFiles';
    
end

fclose('all');

end