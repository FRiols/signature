function rxy = computeXYRatio(x, y)

p1 = x(1) - x(end);
p2 = y(1) - y(end);

rxy = p1 / p2;

end