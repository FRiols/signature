function t = computeTotalTime(time)

t = time(end) - time(1);

end