function [ minX, minY ] = computeMinSpeed(data, speed)
  [~, indexSpeed] = sort(speed);
  minX = zeros(size(data.x, 1), 1);
  minY = zeros(size(data.y, 1), 1);
  
  minX(indexSpeed(1:floor(0.10*size(speed,1)), :)) = data.x(indexSpeed(1:floor(0.10*size(speed,1)), :));
  minY(indexSpeed(1:floor(0.10*size(speed,1)), :)) = data.y(indexSpeed(1:floor(0.10*size(speed,1)), :));
end

