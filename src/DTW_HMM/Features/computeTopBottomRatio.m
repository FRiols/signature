function rhb = computeTopBottomRatio(y)

p1 = 0;
p2 = 0;

for i = 2:length(y)
    
    p1 = p1 + max(y(i) - y(i - 1), 0);
    p2 = p2 + min(y(i) - y(i - 1), 0);
    
end

rhb = -p1 / p2;

end