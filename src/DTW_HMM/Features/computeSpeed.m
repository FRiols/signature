function speed = computeSpeed(x, y, time)

speed = zeros(length(x), 1);

for i = 2:length(x)
    speed(i) = (1000 * (x(i - 1) - x(i)))^2 + (1000 * (y(i - 1) - y(i)))^2;
    dt = time(i) - time(i - 1);
    if (dt == 0)
        dt = 10;
    end
    speed(i) = sqrt(speed(i)) / dt;
end


end