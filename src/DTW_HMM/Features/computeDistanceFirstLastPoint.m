function dist = computeDistanceFirstLastPoint(x, y)

dx = x(end) - x(1);
dy = y(end) - y(1);

dist = sqrt(dx^2 + dy^2);

end