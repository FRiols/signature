function [feat, hPressure] = computeFeatures(data, speed)
%COMPUTEFEATURES Summary of this function goes here
%   Detailed explanation goes here

firstAngle = computeFirstLastAngles(data.x, data.y);
dist = computeDistanceFirstLastPoint(data.x, data.y);
hPressure = computeHighPressurePoints(data.x, data.y, data.pressure);
hMouv = computeHorizontalMouv(data.x);
hSpeed = computeHorzSpeed(speed);
lrRatio = computeLeftRightRatio(data.x);
length = computeLengthSignature(data.x, data.y);
massDim = computeMassDimension(data.x, data.y);
tbRatio = computeTopBottomRatio(data.y);
totalTime = computeTotalTime(data.time);
vertMouv = computeVerticalMouv(data.y);
xyRatio = computeXYRatio(data.x, data.y);
feat = [firstAngle dist hMouv hSpeed lrRatio tbRatio xyRatio length massDim  totalTime vertMouv];

end

