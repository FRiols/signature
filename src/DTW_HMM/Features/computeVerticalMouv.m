function dplctVert = computeVerticalMouv(y)

dplctVert = (max(y) - min(y)) / length(y);

end