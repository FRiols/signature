function dm = computeMassDimension(x, y)

coeff = 10;
invCoeff = 1/coeff;
dmVect = zeros(coeff, 1);
n = 1;

printRect = false;

if (printRect)
    figure('units','normalized');
end

len = length(x);
for i = invCoeff:invCoeff:1
    for j = 1:len
        if (x(j) >= -i) && (x(j) <= i) && (y(j) >= -i) && (y(j) <= i)
            dmVect(n) = dmVect(n) + 1;
        end
    end
    n = n + 1;
    
    % To print the different rectangles
    if (printRect)
        plot(x, y, 'LineWidth', 5);
        hold on;
        plot(x, y, 'go');
        hold on;
        plot([i; i], [i; -i], 'r', 'LineWidth', 5);
        hold on;
        plot([i; -i], [-i; -i], 'r', 'LineWidth', 5);
        hold on;
        plot([-i; -i], [-i; i], 'r', 'LineWidth', 5);
        hold on;
        plot([-i; i], [i; i], 'r', 'LineWidth', 5);
        hold on;
        pause;
    end
end

dmVect = log(dmVect);

dmVect = dmVect ./ log(coeff);
for i = 1:length(dmVect)
    if (dmVect(i) == -Inf)
        dmVect(i) = 0;
    end
end

rectSizes = log(linspace(invCoeff, 1, coeff));

slopes = zeros(length(dmVect) - 1, 1);
for i = 2:length(dmVect)
    slopes(i - 1) = (dmVect(i) - dmVect(i - 1)) / (rectSizes(i) - rectSizes(i - 1));
end

if (printRect)
    figure;
    plot(rectSizes, dmVect);
    hold on;
    plot(rectSizes, dmVect, 'rx');
end

dm = max(slopes);

end