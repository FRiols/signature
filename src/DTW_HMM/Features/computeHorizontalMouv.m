function dplctHorz = computeHorizontalMouv(x)

dplctHorz = (max(x) - min(x)) / length(x);

end