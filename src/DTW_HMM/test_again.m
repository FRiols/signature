function [] = test_again(fileScore)
%% Use the previously computed data, but re-evaluate it
% fileScore: the output file

%% LOAD TRAIN DATA
load('testSaved.mat', 'listTest', 'listTrain', 'numLine');

% Needed for parfor
listTrain = listTrain; %#ok<ASGSL,NODEF>
listTest = listTest; %#ok<ASGSL,NODEF>
numLine = numLine; %#ok<ASGSL,NODEF>


%% PARAMETERS

fn = zeros(3, 1);
fp = zeros(3, 1);
display1 = zeros(2, numLine);


%% READ FILE

fileID = fopen(fileScore, 'w');

ITER_STEP = 100;


for j = 0:idivide(int32(length(listTest) - 1), ITER_STEP)+1
    iter_begin = 1+(ITER_STEP*j);
    iter_end = min(ITER_STEP*(j + 1), length(listTest));

    for i = iter_begin:iter_end
        
      %% TAKE DECISION


      [imp, scDTW, thr] = takeDecision(listTrain{listTest{i}.idListTrain}, listTest{i});

      listTest{i}.impostor = imp;
      listTest{i}.score = scDTW;
      listTest{i}.threshold = thr;

      %% CHECK IF RIGHT

      [dDTW, fn_, fp_] = computeSuccess(listTest{i});

      display1(:, i) = dDTW;
      
      fn = fn + fn_;
      fp = fp + fp_;
      


    end

    fprintf('Iteration number: %i/%i\n', iter_end, numLine);

    %% WRITE INTO FILE
    for i = iter_begin:iter_end
      writeScoreFile(listTest{i}, fileID);
    end
    
end


%surePositive = [76; 68; 56];
%sureNegative = [704; 704; 704];

fprintf('False positive: %i\n', fp(1));
fprintf('False negative: %i\n', fn(1));
%fp = fp * 100 ./ sureNegative
%fn = fn * 100 ./ surePositive


fprintf('Thresholds\n');
for i = 1 : size(listTrain, 1)
    th = listTrain{i}.thresholdDTW1;
    fprintf('%s: %d\n', listTrain{i}.id{1}, floor(th));
end


figure(1);
clf;
hold on;
plot([1 numLine], [-global_thrsh(), -global_thrsh()], 'g');
good = display1(1, :)' ~= 0;
idx = 1:numLine;
plot(idx(good), display1(1, good), 'or', 'LineWidth', 0.1);
plot(idx(~good), display1(2, ~good), 'xb', 'LineWidth', 5);

save('testSaved.mat', 'listTest', 'listTrain', 'numLine');

end

