#! /bin/sh

./evalkit/neweval.pl SCORES/* | grep "EER" | sed 's/.\{33\}$//g' | sed 's/ //g' | tr '\n' ' '
./evalkit/neweval.pl SCORES/* | grep "score" | sed 's/://g' | sed 's/ //g' | tr '\n' ' '

eer=`./evalkit/neweval.pl SCORES/* | grep "EER" | sed 's/.\{33\}$//g'`
score=`./evalkit/neweval.pl SCORES/* | grep "score" | sed 's/://g'`

echo $eer
echo ""
echo $score
echo ""

array=${score//://}


for i in "${!array[@]}", do
    echo "$i=>${array[i]}"
done
