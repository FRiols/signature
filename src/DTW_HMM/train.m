function train(file, trainNumber)

%% LOAD TRAIN FILES LIST

addpath('Features');
[numLine, listTrain] = readTrainFile(file);


%% PARAMETERS

logNumClasses = 4;


%% TRAIN DATA

for i = 1:numLine

    %% LOAD FILE
    TrainVectorDTW = cell(1, size(listTrain{i}.trainFiles, 1));
    FeaturesVector = [];
    for j = 1:size(listTrain{i}.trainFiles, 1)
        data = loadfile(strcat('../../sample/', char(listTrain{i}.trainFiles(j))));
        speed = computeSpeed(data.x, data.y, data.time); 
        [minimumSpeedX, minimumSpeedY] = computeMinSpeed(data, speed);
        acc = computeAcceleration(speed, data.time);
        feat = computeFeatures(data, speed);
        
        %TrainVectorDTW{1, j} = [data.y data.x]';
        if i == 1
            TrainVectorDTW{1, j} = GetDTWVector(data, speed, acc, trainNumber, true, minimumSpeedX, minimumSpeedY)';
        else
            TrainVectorDTW{1, j} = GetDTWVector(data, speed, acc, trainNumber, false, minimumSpeedX, minimumSpeedY)';
        end
        
        FeaturesVector = [FeaturesVector; feat];
    end
    
    listTrain{i}.trainVectorDTW = TrainVectorDTW';
    listTrain{i}.featuresVector = mean(FeaturesVector, 1);
    listTrain{i}.featuresVector = listTrain{i}.featuresVector/ max(listTrain{i}.featuresVector);
    

    %% NORMALISE DATA

    [TRAIN_DTW, ~] = normalizeTrainData(TrainVectorDTW);
    listTrain{i}.train_DTW = TRAIN_DTW;

    %% RUN K-MEANS

%     [idx, code] = runKMeans(DATA_KMEANS_DTW, logNumClasses);
%     listTrain{i}.idx_DTW = idx;
%     listTrain{i}.code_DTW = code;

end

%% SAVE DATA

save('TRAIN.mat', 'listTrain');


%% FIND DTW THRESHOLD

for i = 1:numLine
    scoreDTW1 = 0;
    scoreDTW2 = Inf;
    for j = 1:size(listTrain{i}.trainFiles, 1)
        for k = j + 1:size(listTrain{i}.trainFiles, 1)
            sc = computeDTW(listTrain{i}.train_DTW{1, j}, ...
                listTrain{i}.train_DTW{1, k});
            if sc ~= Inf
                scoreDTW1 = max(scoreDTW1, sc);
                scoreDTW2 = min(scoreDTW2, sc);
            end
        end
    end
    
    if scoreDTW1 == 0
        listTrain{i}.thresholdDTW1 = 90.0;
    else
        listTrain{i}.thresholdDTW1 = scoreDTW1 + 20.0;
    end
    
end

%% Save results

save('TRAIN.mat', 'listTrain');

end
