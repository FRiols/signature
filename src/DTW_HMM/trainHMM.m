function [EST_tr EST_emit] = trainHMM(DATA_KMEANS, logNumClasses, sizeTrEmit)

for i = 1:size(DATA_KMEANS, 1)
    DATA_KMEANS{i} = DATA_KMEANS{i}';
end

num = sizeTrEmit;

EST_TR_init = eye(num, num) * 0.6 + diag(ones(num - 1, 1), 1) * 0.3 ...
   + diag(ones(num - 2, 1), 2) * 0.1;
% EST_TR_init = ones(num, num);

EST_EMIT_init = ones(num, 2^logNumClasses) ./ 2^logNumClasses;

%f = parfeval(@hmmtrain, 2, DATA_KMEANS, EST_TR_init, EST_EMIT_init, ...
%        'TOLERANCE', 10^(-3));
%[EST_tr EST_emit] = fetchOutputs(f);

[EST_tr EST_emit] = hmmtrain(DATA_KMEANS, EST_TR_init, EST_EMIT_init, ...
    'TOLERANCE', 10^(-3));

end
