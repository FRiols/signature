%THR = -1090:10:-1010;
%OUT = cell(1, length(THR));

%i = 1;
%for th = THR

%    train('train.lst', th);
%    test('test.lst', 'score');
%   
%   [err, out] = system('evalkit/neweval.pl score');
%   out = [num2str(THR(i)) out(100:376)]
%   OUT{i} = out;
%   
%   i = i + 1;
%   
%end


%save('THRESHOLD.mat');

for i = 0:2401 + 343 + 49 + 7 - 1
    
    do = 1;
    if (i < 7)
        do = 1;
    elseif (i < 49 + 7)
        if (mod((i - 7), 7) == floor((i - 7) / 7))
            do = 0;
        end
    elseif (i < 343 + 49 + 7)
        if (mod((i - 49 - 7), 7) == mod(floor((i - 49 - 7) / 7), 7))
            do = 0;
        elseif (mod((i - 49 - 7), 7) == floor((i - 49 - 7) / 49))
            do = 0;
        elseif (mod(floor((i - 49 - 7) / 7), 7) == floor((i - 49 - 7) / 49))
            do = 0;
        end
    elseif (i < 2401 + 343 + 49 + 7)
        if (mod((i - 343 - 49 - 7), 7) == mod(floor((i - 343 - 49 - 7) / 7), 7))
            do = 0;
        elseif (mod((i - 343 - 49 - 7), 7) == mod(floor((i - 343 - 49 - 7) / 49)))
            do = 0;
        elseif (mod((i - 343 - 49 - 7), 7) == floor((i - 343 - 49 - 7) / 343))
            do = 0;
        elseif (mod(floor((i - 343 - 49 - 7) / 7), 7) == mod(floor((i - 343 - 49 - 7) / 49)))
            do = 0;
        elseif (mod(floor((i - 343 - 49 - 7) / 7), 7) == floor((i - 343 - 49 - 7) / 343))
            do = 0;
        elseif (mod(floor((i - 343 - 49 - 7) / 49), 7) == floor((i - 343 - 49 - 7) / 343))
            do = 0;
        end 
    end
    
    if (do == 1)
        train('train.lst', i);
        test('test.lst', strcat('SCORES/score', num2str(i)), false, i);
    end
end