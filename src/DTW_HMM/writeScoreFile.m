function writeScoreFile(testEl, fid)
    surete = testEl.score;
    decision = 't';
    if testEl.impostor
        decision = 'f';
    end
    format long;
    fprintf(fid, '%s %s %.0f %s\n', char(testEl.testFile), ...
        char(testEl.versusID), surete, decision);

end
