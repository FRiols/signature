function [TrainVector dv] = initTrainData(trainImages, user, userID)

TrainVector = cell(1, size(trainImages, 2));
dv = cell(2, 2); % Display (debug)

for n = 1:size(trainImages, 2)
    
    % Load and normalize data
    data = loadfile(strcat(user, int2str(userID), '_', int2str(trainImages(n)), '.txt'));
    
    % Store the coordinates (x, y), time, status, azimuth, altitude and
    % pressure
    data.speed = computeSpeed(data);
    acc = computeAcceleration(data.speed, data.time);
    angles = computeAngles(data.x, data.y);
    %hSpeed = computeHorzSpeed(data.x, data.time);
    
    %TrainVector{1, n} = [data.x data.y data.azimuth acc angles data.speed]';
    TrainVector{1, n} = [data.x data.y data.azimuth]';
    %TrainVector{1, n} = [data.x data.y data.pressure data.azimuth data.altitude]';
    
    data2 = normalizeData(data);
    dv{n, 1} = data2;
    
end

end
