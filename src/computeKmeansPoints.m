function points = computeKmeansPoints(data, Code, numClasses)
    mdl=fitcknn(Code,1:numClasses, 'NumNeighbors',1);
    points = predict(mdl, data);
end